import json
import glob
import wget
from multiprocessing.dummy import Pool as ThreadPool
pool = ThreadPool(4)

jsons = glob.glob("*.json")

def download(to_download):
    wget.download(to_download[0], to_download[1])

to_download = []
for j_file in jsons:
    with open(j_file) as f:
        datas = json.load(f)

    data = datas['results']
    count = 0
    for entry in data:
        m_count = 0
        for mechanism in entry['reaction']['mechanisms']:
            m_count += 1
            for step in mechanism['steps']:
                if len(step['marvin_xml']) > 2:
                    to_download.append([entry['mcsa_id'], m_count, mechanism['rating'], step['step_id'], step['marvin_xml']])

downloads_links = []
for d in to_download:
    if d[0] < 10:
        entry = "000" + str(d[0])
    elif d[0] >= 10 and d[0] < 100:
        entry = "00" + str(d[0])
    elif d[0] >= 100 and d[0] < 1000:
        entry = "0" + str(d[0])
    else:
        entry = str(d[0])

    if d[1] < 10:
        mechanisms = "0" + str(d[1])
    else:
        mechanisms = str(d[1])

    rating = str(d[2])
    
    if d[3] < 10:
        step = "0" + str(d[3])
    else:
        step = str(d[3])

    new_fname = "Steps/" + "Step_mcsa" + "." + entry + "." + mechanisms + "." + step + "." + "[" + rating + "]" + ".mrv"
    url = d[-1]
    if "https://" not in url:
        url = "https://" + url

    downloads_links.append([url, new_fname])

pool.map(download, downloads_links)
pool.close()
pool.join()