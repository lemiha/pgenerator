import subprocess
import glob
import os
import numpy as np
import sys
from multiprocessing.dummy import Pool as ThreadPool
pool = ThreadPool(62)

counter = 0
args = sys.argv

files = []
for file in glob.glob(args[1] + "/*.mrv"):
    files.append(file)

files.sort()

# Perform the conversion
folder = args[1].split("/")[0]

if "product" in args[1]:
    if "id" in args[1]:
        outf = folder + "/smiles_products_id/" + ".smi"
    else:
        outf = folder + "/smiles_products/" + ".smi"
else: # reactant
    if "id" in args[1]:
        outf = folder + "/smiles_reactants_id/" + ".smi"
    else:
        outf = folder + "/smiles_reactants/" + ".smi"

# Remove existing files in folder
for f in glob.glob(outf.split(".smi")[0] + "*"):
    os.remove(f)

# Perform the conversion
orginal = args[1] + "/*.mrv"

if args[2] == "no_stereo":
    command = "molconvert" + " " + "smiles:0" + " " + orginal + " " + "-o" + " " + str(outf) + " " + "-m"
    print(command)
else:
    command = "molconvert" + " " + "smiles" + " " + orginal + " " + "-o" + " " + str(outf) + " " + "-m"

process = subprocess.run(command, shell=True)

# Rename files, such that they can be sorted
out_dst = outf.split(".smi")[0] + "*.smi"
out_files = []

for file in glob.glob(out_dst):
    tmp = file.split(".smi")[0].split("/")

    if len(tmp[2]) == 1:
        tmp[2] = "000" + tmp[2]
    elif len(tmp[2]) == 2:
        tmp[2] = "00" + tmp[2]
    elif len(tmp[2]) == 3:
        tmp[2] = "0" + tmp[2]

    newf_name = "/".join(tmp) + ".smi"

    os.rename(file, newf_name)
    out_files.append(newf_name)

out_files.sort()

# Rename files correctly
out_dst = outf.split(".smi")[0]

for i in range(len(files)):
    tmp = files[i].split(".mrv")[0].split("/")

    f_dst = out_dst + tmp[2] + ".smi"

    os.rename(out_files[i], f_dst)

