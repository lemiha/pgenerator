import numpy as np
import sys
import glob
from converttomcsa_methods import Update
from multiprocessing.dummy import Pool as ThreadPool
pool = ThreadPool(2)

u = Update()

files = []
for file in glob.glob("explicit_data/reactants/*.mrv"):
    files.append(file)

files.sort()

def convert(fname):
    with open(fname) as f:
        content = f.readlines()
    del content[0]

    u = Update()

    if "<MDocument><MChemicalStruct><molecule molID=\"m1\"><atomArray " in content[0]:
        u.updateDocument1(fname, content, fname)
    if "<MDocument><MChemicalStruct><molecule molID=\"m1\" absStereo=\"true\"><atomArray atomID=" in content[0]:
        u.updateDocument1(fname, content, fname)
    if "<MDocument><MChemicalStruct><molecule molID=\"m1\"><atomArray><atom" in content[0]:
        u.updateDocument2(fname, content, fname)
    if "<MDocument><MChemicalStruct><molecule molID=\"m1\" absStereo=\"true\"><atomArray><atom" in content[0]:
        u.updateDocument2(fname, content, fname)


pool.map(convert, files)
pool.close()
pool.join()

