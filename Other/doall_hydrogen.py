import subprocess
from multiprocessing.dummy import Pool as ThreadPool
pool = ThreadPool(2)

def convert(src):
    subprocess.run(src)

subprocess.run(["python3", "doremove_hydrogen_folders.py"])

# 1. Create the nesseary folders if they doesn't exist
print(1)
subprocess.run(["python3", "makefolders.py"])

# 2. Make two datasets. One where the hydrogen is implicit and one where it is explicit 
print(2)
subprocess.run(["python3", "makedata.py", "explicit"])

# 3. The generated explicit marvin files are not in the same structure as the files from the M-CSA database, hence it is converted to the same structure
print(3)
subprocess.run(["python3", "converttomcsa.py"])

# 4a. Generate the products/output of the mechanistic steps
subprocess.run(["python3", "keep_only_one_h.py", "explicit_data/reactants"])

# 4b. Generate the products/output of the mechanistic steps
print(4)
subprocess.run(["python3", "perform_reaction.py", "explicit_data_oneh/reactants"])

# 5. 
print(5)
subprocess.run(["python3", "update_mrv.py", "explicit_data_oneh/reactants"])
subprocess.run(["python3", "update_mrv.py", "explicit_data_oneh/products"])

# 6.
print(6)
subprocess.run(["python3", "replacestuff.py", "explicit_data_oneh/reactants"])
subprocess.run(["python3", "replacestuff.py", "explicit_data_oneh/reactants_id"])
subprocess.run(["python3", "replacestuff.py", "explicit_data_oneh/products"])
subprocess.run(["python3", "replacestuff.py", "explicit_data_oneh/products_id"])

# 7.
print(7)
commands = []
commands.append(["python3", "convert_to_smiles.py", "explicit_data_oneh/reactants", "no_stereo"])
commands.append(["python3", "convert_to_smiles.py", "explicit_data_oneh/reactants_id", "no_stereo"])
commands.append(["python3", "convert_to_smiles.py", "explicit_data_oneh/products", "no_stereo"])
commands.append(["python3", "convert_to_smiles.py", "explicit_data_oneh/products_id", "no_stereo"])

pool.map(convert, commands)
pool.close()
pool.join()

# 8
print(8)
subprocess.run(["python3", "replacewithstar_and_oh.py", "explicit_data_oneh/smiles_reactants"])
subprocess.run(["python3", "replacewithstar_and_oh.py", "explicit_data_oneh/smiles_reactants_id"])
subprocess.run(["python3", "replacewithstar_and_oh.py", "explicit_data_oneh/smiles_products"])
subprocess.run(["python3", "replacewithstar_and_oh.py", "explicit_data_oneh/smiles_products_id"])

# 9
print(9)
subprocess.run(["python3", "gatherdata.py", "explicit_h"])

# 10
print(10)
subprocess.run(["python3", "validate_molecules.py"])

# BACK: