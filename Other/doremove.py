import subprocess
import glob, os

# # Make the reactions for both the mrv-files with and without atom-atom mapping
paths = []
paths.append('explicit_data/reactants/*')
paths.append('explicit_data/reactants_id/*')
paths.append('explicit_data/products_id/*')
paths.append('explicit_data/products/*')

for path in paths:
    for f in glob.glob(path):
        os.remove(f)