import subprocess
import glob, os

# # Make the reactions for both the mrv-files with and without atom-atom mapping
paths = []
paths.append('explicit_data_oneh/reactants/*')
paths.append('explicit_data_oneh/reactants_id/*')
paths.append('explicit_data_oneh/products_id/*')
paths.append('explicit_data_oneh/products/*')


for path in paths:
    for f in glob.glob(path):
        os.remove(f)