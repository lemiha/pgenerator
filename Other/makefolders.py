import subprocess
import os, glob

paths = []

# Normal
paths.append('./explicit_data/reactants_id')
paths.append('./explicit_data/products_id')
paths.append('./explicit_data/reactants')
paths.append('./explicit_data/products')

paths.append('./explicit_data/smiles_reactants_id')
paths.append('./explicit_data/smiles_products_id')
paths.append('./explicit_data/smiles_reactants')
paths.append('./explicit_data/smiles_products')

# Hydrogen
paths.append('./explicit_data_oneh/reactants_id')
paths.append('./explicit_data_oneh/products_id')
paths.append('./explicit_data_oneh/reactants')
paths.append('./explicit_data_oneh/products')
paths.append('./explicit_data_oneh/smiles_reactants_id')
paths.append('./explicit_data_oneh/smiles_products_id')
paths.append('./explicit_data_oneh/smiles_reactants')
paths.append('./explicit_data_oneh/smiles_products')

for path in paths:
    if os.path.exists(path) == False:
        os.mkdir(path)