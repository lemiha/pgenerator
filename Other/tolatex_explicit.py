import glob
import subprocess
import os
import sys

args = sys.argv

fname = "toprint_explicit/failed_pictures.tex"
latex_outf = open(fname, "w+")

reactant_files = []
for file in glob.glob('toprint_explicit/png_reactants_id/*.png'):
    reactant_files.append(file)
reactant_files.sort()

product_files = []
for file in glob.glob('toprint_explicit/png_products_id/*.png'):
    product_files.append(file)
product_files.sort()

com_files = []
for file in glob.glob('toprint_explicit/reactants_id_com_png/*.png'):
    com_files.append(file)
com_files.sort()

# Should be the same for all
num_of_files = len(reactant_files)

for i in range(num_of_files):
    name_reac = reactant_files[i].split("Step_mcsa.")[1].split("_id.png")[0]
    name_prod = product_files[i].split("Step_mcsa.")[1].split("._ur_id.png")[0]
    name_com = com_files[i].split("Step_mcsa.")[1].split("_id.png")[0]

    latex_outf.write("\\begin{minipage}{0.5\\textwidth}" + "\n")
    latex_outf.write("\t\\begin{figure}[H]" + "\n")
    latex_outf.write("\t\t\\captionsetup{justification=centering,margin=2cm}" + "\n")
    latex_outf.write("\t\t\\centering" + "\n")
    
    tmp = reactant_files[i].split("/")
    tmp_reac = tmp[1] + "/""{" + tmp[2].replace(".png", "") + "}.png"
    latex_outf.write("\t\t\\includegraphics[width=0.9\\linewidth]{"+ str(tmp_reac) +"}" + "\n")
    latex_outf.write("\t\t\\caption{"+ str(name_reac) +"}" + "\n")
    latex_outf.write("\t\\end{figure}" + "\n")
    latex_outf.write("\\end{minipage}" + "\n")
    latex_outf.write("\\begin{minipage}{0.5\\textwidth}" + "\n")
    latex_outf.write("\t\\begin{figure}[H]" + "\n")
    latex_outf.write("\t\t\\centering" + "\n")
    latex_outf.write("\t\t\\begin{subfigure}[b]{0.55\\textwidth}" + "\n")
    
    tmp = product_files[i].split("/")
    tmp_prod = tmp[1] + "/""{" + tmp[2].replace(".png", "") + "}.png"
    latex_outf.write("\t\t\\includegraphics[width=1\\linewidth]{"+ str(tmp_prod) +"}" + "\n")
    latex_outf.write("\t\t\\caption{"+ "My Implementation" +"}" + "\n")
    latex_outf.write("\t\t\\end{subfigure}" + "\n")
    latex_outf.write("\t\t\\begin{subfigure}[b]{0.55\\textwidth}" + "\n")
    
    tmp = com_files[i].split("/")
    tmp_com = tmp[1] + "/""{" + tmp[2].replace(".png", "") + "}.png"
    latex_outf.write("\t\t\\includegraphics[width=1\\linewidth]{"+ str(tmp_com) +"}" + "\n")
    latex_outf.write("\t\t\\caption{Their: "+ str(name_com) +"}" + "\n")
    latex_outf.write("\t\t\\end{subfigure}" + "\n")
    latex_outf.write("\t\\end{figure}" + "\n")
    latex_outf.write("\\end{minipage}" + "\n")

    if i != num_of_files-1:
        latex_outf.write("\n\\newpage \n\n")

latex_outf.close()

subprocess.run('(cd toprint_explicit ; pdflatex main.tex)', shell=True)

save_fname = args[1].split("_")[1].split(".")[0]
os.rename("toprint_explicit/main.pdf", "toprint_explicit/"+save_fname +".pdf")
    