import glob
import re
import numpy as np
import os
import subprocess
import sys

args = sys.argv

# Remove files
r_paths = []
r_paths.append('toprint_explicit/reactants_id/*')
r_paths.append('toprint_explicit/reactants_id_com/*')
r_paths.append('toprint_explicit/products_id/*')
r_paths.append('toprint_explicit/png_reactants_id/*')
r_paths.append('toprint_explicit/reactants_id_com_png/*')
r_paths.append('toprint_explicit/png_products_id/*')

for path in r_paths:
    for f in glob.glob(path):
        os.remove(f)

# Reactants smiles files
reactant_files = []
for file in glob.glob('explicit_data/reactants_id/*.mrv'):
    reactant_files.append(file)

for i in range(len(reactant_files)):
    tmp = reactant_files[i].split(".")
    tmp_f = tmp[1] + "." + tmp[2] + "." + tmp[3][0:2]
    reactant_files[i] = [tmp_f, reactant_files[i]]

reactant_files = np.array(reactant_files)

# Products smiles files
products_files = []
for file in glob.glob('explicit_data/products_id/*.mrv'):
    products_files.append(file)

for i in range(len(products_files)):
    tmp = products_files[i].split(".")
    tmp_f = tmp[1] + "." + tmp[2] + "." + tmp[3][0:2]
    products_files[i] = [tmp_f, products_files[i]]

products_files = np.array(products_files)

# Load the correct smiles
file_to_print = args[1]
with open(file_to_print) as f:
    f_tmp_fnames = f.readlines()

f_tmp_fnames = [x.strip() for x in f_tmp_fnames]

failed_files1 = []
failed_files2 = []
for line in f_tmp_fnames:
    tmp_f1 = line.split(";")[0].split(".")
    tmp_f1 = tmp_f1[1] + "." + tmp_f1[2] + "." + tmp_f1[3][0:2]
    failed_files1.append(tmp_f1)

for line in f_tmp_fnames:
    tmp_f2 = line.split(";")[1].split(".")
    tmp_f2 = tmp_f2[1] + "." + tmp_f2[2] + "." + tmp_f2[3][0:2]
    failed_files2.append(tmp_f2)

found_files = []
for f_file in failed_files1:
    r_index = np.where(reactant_files[:,0] == f_file)[0][0]
    p_index = np.where(products_files[:,0] == f_file)[0][0]

    found_files.append(reactant_files[r_index,1])
    found_files.append(products_files[p_index,1])

compared_files = []
for f_file in failed_files2:
    r_index = np.where(reactant_files[:,0] == f_file)[0][0]

    compared_files.append(reactant_files[r_index,1])


for file in found_files:
    with open(file) as f:
        content = f.readlines()

    for i in range(len(content)):
        if "mrvExtraLabel=\"" in content[i]:
            content[i] = re.sub(' mrvExtraLabel=".*?"', '', content[i])

        if "mrvAlias=\"" in content[i]:
            content[i] = re.sub(' mrvAlias=".*?"', '', content[i])

    tmp = file.split("/")
    tmp[0] = "toprint_explicit"

    out_fname = "/".join(tmp)
    
    outf = open(out_fname, 'w+')

    for line in content:
        outf.write(line)
    
    outf.close()

for file in compared_files:
    with open(file) as f:
        content = f.readlines()

    for i in range(len(content)):
        if "mrvExtraLabel=\"" in content[i]:
            content[i] = re.sub(' mrvExtraLabel=".*?"', '', content[i])

        if "mrvAlias=\"" in content[i]:
            content[i] = re.sub(' mrvAlias=".*?"', '', content[i])

    tmp = file.split("/")

    out_fname = "toprint_explicit/reactants_id_com/" + tmp[2]
    
    outf = open(out_fname, 'w+')

    for line in content:
        outf.write(line)
    
    outf.close()

# Convert all to png
paths = [['toprint_explicit/reactants_id/*.mrv', 'toprint_explicit/png_reactants_id/'], ['toprint_explicit/products_id/*.mrv', 'toprint_explicit/png_products_id/'], ['toprint_explicit/reactants_id_com/*.mrv', 'toprint_explicit/reactants_id_com_png/']]

for path in paths:
    # Convert all files
    command = "molconvert" + " " + "png:w1500,anum,atsiz0.25,#00ffffff" + " " + str(path[0]) + " " + "-o" + " " + str(path[1]) + ".png" + " " + "-m"
    subprocess.run(command, shell=True)

    # Rename files, such that they can be sorted
    srf_files = []
    for file in glob.glob(path[0]):
        srf_files.append(file)

    srf_files.sort()

    out_dst = path[1] + "*.png"
    out_files = []
    for file in glob.glob(out_dst):
        tmp = file.split(".png")[0].split("/")
        # print(tmp)
        if len(tmp[2]) == 1:
            tmp[2] = "000" + tmp[2]
        elif len(tmp[2]) == 2:
            tmp[2] = "00" + tmp[2]
        elif len(tmp[2]) == 3:
            tmp[2] = "0" + tmp[2]

        newf_name = "/".join(tmp) + ".png"

        os.rename(file, newf_name)
        out_files.append(newf_name)

    out_files.sort()

    # Rename files correctly
    for i in range(len(srf_files)):
        tmp = srf_files[i].split("/")
        f_dst = path[1] + tmp[2].split('.mrv')[0] + ".png"
        
        os.rename(out_files[i], f_dst)

        
        