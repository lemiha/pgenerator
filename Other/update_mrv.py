import numpy as np
import sys
import re
import glob, os

args = sys.argv

files = []

if "id" in args[1]:
    print("Only use folder without id")
else:
    for file in glob.glob(args[1] + "/*.mrv"):
        files.append(file)

    # First remove possible mrvMap if they already exists
    for fname in files:
        with open(fname) as f:
            content = f.readlines()

        for i in range(len(content)):
            if "mrvMap=\"" in content[i]:
                content[i] = re.sub(' mrvMap=".*?"', '', content[i])

        fout = open(fname, "w+")

        for line in content:
            fout.write(line)
    

    # Add id's to the files
    for file in files:
        fname = file

        print(fname)

        with open(fname) as f:
            content = f.readlines()

        content = [x.strip() for x in content] 

        list_of_atoms = []
        list_of_bonds = []

        skip = False
        for i in range(len(content)):
            if ("mrvMap=" in content[i]):
                skip = True
                break
            if ("<atom id=" in content[i]):
                atomid = content[i].split("<atom id=\"")[1].split("\" el")[0].split("a")[1]
                tmp = content[i].split("x2=")
                content[i] = tmp[0] + "mrvMap=\"" + atomid + "\" x2=" + tmp[1]

        if skip == True:
            continue

        dst_folder = args[1].split("/")
        tmp = file.split("/")
        f1 = tmp[0]
        f2 = tmp[1] + "_id"
        tmp_name = tmp[2].split(".mr")[0]


        fout = f1 + "/" + f2 + "/" + tmp_name + "_id" + ".mrv"
        
        with open(fout, 'w+') as f:
            for line in content:
                f.write("%s\n" % line)