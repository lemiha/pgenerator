
import rdkit.Chem as Chem

files = []
files.append(["gathered_data_explicit_withname.txt", "gathered_data_explicit.txt"])
files.append(["gathered_data_explicit_h_withname.txt", "gathered_data_explicit_h.txt"])

# TMPDIFF
for f_pair in files:
    print(f_pair)
    with open(f_pair[0]) as f:
        content0 = f.readlines()

    with open(f_pair[1]) as f:
        content1 = f.readlines()

    c = 0
    reacs = []
    for line in content0:
        tmp = line.split(">>")[0].split("  ")
        print(tmp)
        
        rmols = tmp[1].split(".")
        pmols = line.split(">>")[1].split(" ")[0].split(".")

        reacs.append([rmols, pmols])

        c += 1

    for i in reversed(range(len(reacs))):
        skip = False
        for rmol in reacs[i][0]:
            tmp = Chem.MolFromSmiles(rmol, sanitize = False)
            try:
                tmp.UpdatePropertyCache()
                Chem.GetSymmSSSR(tmp)
            except:
                print(i+1)
                skip = True
            
        if skip == True:
            del content0[i]
            del content1[i]
        else:
            for pmol in reacs[i][1]:
                tmp = Chem.MolFromSmiles(pmol, sanitize = False)
                try:
                    tmp.UpdatePropertyCache()
                    Chem.GetSymmSSSR(tmp)
                except:
                    print(i+1)
                    del content0[i]
                    del content1[i]

    fout0 = open(f_pair[0], "w+")
    fout1 = open(f_pair[1], "w+")

    for line in content0:
        fout0.write(line)

    for line in content1:
        fout1.write(line)
        
    fout0.close()
    fout1.close()