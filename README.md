# PGenerator

PGenerator is a graph transformation framework for extracting mechanistic correct atom-atom mapping from the M-CSA database of enzymatic reactions.

## Acquiring the Marvin files

Inside the folder <tt>Data_Extraction</tt>, the script <tt>extract_data.py</tt> is found. Running the script downloads all the Marvin files from the M-CSA database into the folder <tt>Steps</tt>.

## Main Scripts

Running the file <tt>doall.py</tt> runs the entire application to generate all the reaction SMILES with mechanistically correct atom-atom mappings. This script produces the datasets <tt>gathered_data_explicit.txt</tt> and <tt>gathered_data_explicit_withname.txt</tt>. Both have the same content, but the latter also contains information about the entry, mechanism, step and rating for each reaction SMILES. The datasets contain all molecules of the mechanistic steps and all hydrogen are explicit.

In the folder <tt>Other</tt>, running the file <tt>doall_hydrogen.py</tt> produces the datasets <tt>gathered_data_explicit_h.txt</tt> and <tt>gathered_data_explicit_h_withname.txt</tt>. These datasets contain all molecules of the mechanistic steps, but here at most <em>one</em> hydrogen are explicit.
 
The manipulation of the electron flow is done in <tt>perform_reaction.py</tt> and <tt>manipulations.py</tt>. The conversion to SMILES is done in <tt>convert_to_smiles.py</tt>. The validation is done in <tt>compare_smiles.py</tt> and <tt>compare_methods.py</tt>. All the data is combined in <tt>gatherdata.py</tt>.
