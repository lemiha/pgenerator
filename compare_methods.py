import glob
import numpy as np
from manipulations import Reaction
from collections import Counter

class Compare:
    def __init__(self):
        self.left_counters = []
        self.right_counters = []
        self.left_hyrdrogen_counters = []
        self.right_hyrdrogen_counters = []
        self.diff_size = []
        self.diff_true_indices = []
        self.equal_size_fail = []
        self.diff_size_fail = []
         
        # Charge counters
        self.left_charge_counters = [] 
        self.right_charge_counters = []

        # Bond counters
        self.left_bond_counters = []
        self.right_bond_counters = []
        self.left_stereo_counters = []
        self.right_stereo_counters = []

        # Counters
        self.count_true = 0        
        self.count_tmp = 0
        self.count_equal_true = 0
        
        # Files
        self.f_failed = open("failed_smiles.txt","w+")
        self.f_true = open("correct_smiles.txt","w+")
        self.f_empthy_fail = open("fails_empthy.txt","w+")
        self.f_charge_fail = open("fails_charge.txt","w+")
        self.f_coor_fail = open("fails_coor.txt","w+")
        self.f_bond_fail = open("fails_bond.txt","w+")
        self.f_uncategorised_fail = open("fails_uncategorised.txt","w+")
        self.f_nr_of_matches = open("nr_of_matches.txt","a")

        self.empthy_fail = []
        self.charge_fail = []
        self.coor_fail = []
        self.bond_fail = []
        self.stereo_fails = []
        self.uncategorised_fails = []

    # Load data
    def loadData(self, args, compare_reactants):
        product_files = []
        react_files = []

        if compare_reactants == False:
            for file in glob.glob(args[1] + "_data" + "/smiles_products/*.smi"):
                product_files.append(file)
        else:
            for file in glob.glob(args[1] + "_data" + "/smiles_reactants/*.smi"):
                product_files.append(file)

        for file in glob.glob(args[1] + "_data" + "/smiles_reactants/*.smi"):
            react_files.append(file)

        product_files.sort()
        react_files.sort()

        product_array = []
        for fname in product_files:
            if ".." in fname:
                tmp_prod = fname.split("..")[1].split(".")
            else:
                tmp_prod = fname.split(".")       

            prod_id2 = int(tmp_prod[2])
            prod_id1 = tmp_prod[1]
            prod_id3 = int(tmp_prod[3].split("_")[0])

            if "a" in prod_id1:
                product_array.append([int(prod_id1.replace("a","")), 1, prod_id2, prod_id3])
            elif "b" in prod_id1:
                product_array.append([int(prod_id1.replace("b","")), 2, prod_id2, prod_id3])
            else:
                product_array.append([int(prod_id1), 0, prod_id2, prod_id3])


        react_array = []
        for fname in react_files:
            if ".." in fname:
                tmp_react = fname.split("..")[1].split(".")
            else:
                tmp_react = fname.split(".")

            react_id1 = tmp_react[1]
            react_id2 = int(tmp_react[2])
            react_id3 = int(tmp_react[3].split("_")[0])

            if "a" in react_id1:
                react_array.append([int(react_id1.replace("a","")), 1, react_id2, react_id3])
            elif "b" in react_id1:
                react_array.append([int(react_id1.replace("b","")), 2, react_id2, react_id3])
            else:
                react_array.append([int(react_id1), 0, react_id2, react_id3])

        product_array = np.array(product_array)
        react_array = np.array(react_array)

        indices = []
        product_indices = list(range(len(product_array)))
        for i in range(len(react_array)):
            # print(react)
            r1 = react_array[i][0]
            r2 = react_array[i][1]
            r3 = react_array[i][2]
            r4 = react_array[i][3]

            for j in range(len(product_indices)):
                p1 = product_array[product_indices[j]][0]
                p2 = product_array[product_indices[j]][1]
                p3 = product_array[product_indices[j]][2]
                p4 = product_array[product_indices[j]][3]
                
                if r1 == p1 and r2 == p2 and r3 == p3 and r4 == p4+1:
                    indices.append([i,product_indices[j]])
                    del product_indices[j]
                    break

        rarray = []
        for pair in indices:
            rarray.append([product_files[pair[1]], react_files[pair[0]]])

        tmp_array = []
        for reac in rarray:
            tmp = []
            for smi_file in reac:
                f_failed = open(smi_file)
                tmp_smiles = f_failed.readline()
                tmp_smiles = tmp_smiles.strip()
                tmp.append(tmp_smiles)
            f_failed.close()

            tmp_array.append(tmp)

        sarray = []
        failed_notation = []


        i = -1
        for reac in tmp_array:
            i += 1
            left = reac[0].strip().split(".")
            right = reac[1].strip().split(".")

            left.sort()
            right.sort()

            sarray.append([left, right])

        return rarray, sarray
    
    def getDiffIndices(self):
        return self.diff_true_indices
        

    def checkForChange(self, f_prod, f_reac):
        with open(f_prod) as f:
            prod_content = f.readlines()

        with open(f_reac) as f:
            reac_content = f.readlines()

        prod_labels = []
        for line in prod_content:
            if "mrvExtraLabel" in line:
                tmp_label = line.split("mrvExtraLabel=\"")[1].split("\"")[0]
            elif "mrvAlias" in line:
                tmp_label = line.split("mrvAlias=\"")[1].split("\"")[0]
            else:
                continue

            tmp_atomid = line.split("<atom id=\"")[1].split("\" el")[0].split('a')[1]
            tmp_atomid = int(tmp_atomid)
            tmp_atomname = line.split("elementType=\"")[1].split("\"")[0]

            prod_labels.append([tmp_atomid, tmp_atomname, tmp_label])

        reac_labels = []
        for line in reac_content:
            line = line.strip()

            if "mrvExtraLabel" in line:
                tmp_label = line.split("mrvExtraLabel=\"")[1].split("\"")[0]
            elif "mrvAlias" in line:
                tmp_label = line.split("mrvAlias=\"")[1].split("\"")[0]
            else:
                continue

            tmp_atomid = line.split("<atom id=\"")[1].split("\" el")[0].split('a')[1]
            tmp_atomid = int(tmp_atomid)
            # print(tmp_label)
            tmp_atomname = line.split("elementType=\"")[1].split("\"")[0]

            reac_labels.append([tmp_atomid, tmp_atomname, tmp_label])

        prod_labels = np.array(prod_labels)
        reac_labels = np.array(reac_labels)

        return prod_labels, reac_labels

    def findChargesMismatch(self, sarray, rarray):
        failed_indices = []

        for i in range(len(sarray)):
            left = np.array(sarray[i][0])
            right = np.array(sarray[i][1])

            tmp_left = []
            tmp_right = []
            tmp_charge_left = []
            tmp_charge_right = []
            tmp_hyrogen_left = []
            tmp_hyrogen_right = []
            tmp_bond_left = []
            tmp_bond_right = []
            tmp_sterop_left = []
            tmp_stereo_right = []

            for j in range(len(left)):
                tmp = Counter(left[j])
                tmp_left.append([tmp['*'], tmp['As'], tmp['Br'], tmp['C'], tmp['CH'], tmp['Ca'], tmp['Cl'], tmp['Co'], tmp['Cu'], tmp['F'], tmp['Fe'], tmp['Hg'], tmp['I'], tmp['K'], tmp['Mg'], tmp['Mn'], tmp['Mo'], tmp['N'], tmp['NH'], tmp['Ni'], tmp['O'], tmp['OH'], tmp['P'], tmp['S'], tmp['SH'], tmp['Se'], tmp['V'], tmp['Zn'], tmp['c'], tmp['n'], tmp['n'], tmp['o'], tmp['nH'], tmp['s']])
                tmp_charge_left.append([tmp["+"], tmp["++"], tmp["+3"], tmp["+4"], tmp["+5"], tmp["+6"], tmp["-"], tmp["--"], tmp["-3"], tmp["-4"]])
                tmp_hyrogen_left.append(tmp["H"])
                tmp_bond_left.append([tmp["="], tmp["#"]])
                tmp_sterop_left.append([tmp["@"], tmp["[C@]"], tmp["[S@]"], tmp["[C@@]"], tmp["[S@@]"], tmp["\\"]])
            for j in range(len(right)):
                tmp = Counter(right[j])
                tmp_right.append([tmp['*'], tmp['As'], tmp['Br'], tmp['C'], tmp['CH'], tmp['Ca'], tmp['Cl'], tmp['Co'], tmp['Cu'], tmp['F'], tmp['Fe'], tmp['Hg'], tmp['I'], tmp['K'], tmp['Mg'], tmp['Mn'], tmp['Mo'], tmp['N'], tmp['NH'], tmp['Ni'], tmp['O'], tmp['OH'], tmp['P'], tmp['S'], tmp['SH'], tmp['Se'], tmp['V'], tmp['Zn'], tmp['c'], tmp['n'], tmp['n'], tmp['o'], tmp['nH'], tmp['s']])
                tmp_charge_right.append([tmp["+"], tmp["++"], tmp["+3"], tmp["+4"], tmp["+5"], tmp["+6"], tmp["-"], tmp["--"], tmp["-3"], tmp["-4"]])
                tmp_hyrogen_right.append(tmp["H"])
                tmp_bond_right.append([tmp["="], tmp["#"]])
                tmp_stereo_right.append([tmp["@"], tmp["[C@]"], tmp["[S@]"], tmp["[C@@]"], tmp["[S@@]"], tmp["\\"]])

            self.left_charge_counters.append(np.array(tmp_charge_left))
            self.right_charge_counters.append(np.array(tmp_charge_right))
            self.left_bond_counters.append(np.array(tmp_bond_left))
            self.right_bond_counters.append(np.array(tmp_bond_right))
            self.left_stereo_counters.append(np.array(tmp_sterop_left))
            self.right_stereo_counters.append(np.array(tmp_stereo_right))
            self.left_counters.append(np.array(tmp_left))
            self.right_counters.append(np.array(tmp_right))
            self.left_hyrdrogen_counters.append(np.array(tmp_hyrogen_left))
            self.right_hyrdrogen_counters.append(np.array(tmp_hyrogen_right))

        self.left_charge_counters = np.array(self.left_charge_counters)
        self.right_charge_counters = np.array(self.right_charge_counters)
        self.left_bond_counters = np.array(self.left_bond_counters)
        self.right_bond_counters = np.array(self.right_bond_counters)
        self.left_stereo_counters = np.array(self.left_stereo_counters)
        self.right_stereo_counters = np.array(self.right_stereo_counters)
        self.left_counters = np.array(self.left_counters)
        self.right_counters = np.array(self.right_counters)
        self.left_hyrdrogen_counters = np.array(self.left_hyrdrogen_counters)
        self.right_hyrdrogen_counters = np.array(self.right_hyrdrogen_counters)
            

        # Sort out all the fails with mismatches in charges and bonds
        for i in range(len(self.left_counters)):
            left = np.array(sarray[i][0])
            right = np.array(sarray[i][1])

            if len(left) == len(right):
                for j in range(len(left)):
                    if left[j] != right[j]:
                        failed_indices.append(i)
                        self.equal_size_fail.append(i)
                        break
                    if j == len(left)-1:
                        self.f_true.write(str(rarray[i][0])  + "\n")
                        self.count_equal_true += 1
                        self.count_true += 1
            else:
                self.diff_size.append(i)

    def checkCharge(self, left, right, left_charges, right_charges):
        for k in range(len(left_charges)):
            if np.array_equal(left_charges[k], right_charges[k]) == False:
                return True
            
            if k == len(left_charges)-1:
                return False

    def checkCoordinate(self, r_e):
        is_coor = False
        tofind_file = r_e[0].split(".")
        tofind_file = ".".join(tofind_file[1:4])

        reac_file = glob.glob("explicit_data/reactants/*"+ tofind_file +"*.mrv")
        prod_file = glob.glob("explicit_data/products/*"+ tofind_file +"*.mrv")
        
        if len(reac_file) == 0 or len(prod_file) == 0:
            return is_coor

        r = Reaction()
        p = Reaction()
        r.initialize(reac_file[0])
        p.initialize(prod_file[0])

        reac_atoms = r.getReactingAtoms()
        
        for atom_id in reac_atoms:
            reac_bond_ids = r.getAllNeighboringBonds(atom_id)
            reac_bond_types = r.list_of_bonds[reac_bond_ids]
            reac_coor_indices = np.where(reac_bond_types[:,2] == -5)[0]
            
            reac_neigh_atomids = np.concatenate([reac_bond_types[reac_coor_indices][:,0], reac_bond_types[reac_coor_indices][:,1]])
            reac_neigh_atomids = np.unique(reac_neigh_atomids)
            reac_neigh_atomids = reac_neigh_atomids[reac_neigh_atomids != atom_id]

            prod_bond_ids = p.getAllNeighboringBonds(atom_id)
            prod_bond_types = p.list_of_bonds[prod_bond_ids]
            prod_coor_indices = np.where(prod_bond_types[:,2] == -5)[0]

            prod_neigh_atomids = np.concatenate([prod_bond_types[prod_coor_indices][:,0], prod_bond_types[prod_coor_indices][:,1]])
            prod_neigh_atomids = np.unique(prod_neigh_atomids)
            prod_neigh_atomids = prod_neigh_atomids[prod_neigh_atomids != atom_id]
            
            intersect = np.intersect1d(reac_neigh_atomids,prod_neigh_atomids) 

            if len(intersect) != len(reac_neigh_atomids) or len(intersect) != len(prod_neigh_atomids):
                return 1

            if True in np.isin(reac_neigh_atomids, reac_atoms) or True in np.isin(prod_neigh_atomids, reac_atoms):
                return 2
        
        return 0
        
    def checkBond(self, left_bond, right_bond):
        for k in range(len(left_bond)):
            if np.array_equal(left_bond[k], right_bond[k]) == False:
                return True
            
            if k == len(left_bond)-1:
                return False

    def checkCorrected(self, left, right):
        for j in range(len(left)):
            if np.array_equal(left[j],right[j]) == False:        
                return False
            
            if j == len(left)-1:
                return True

    def checkStereo(self, left_bond, right_bond, left, right):
        for k in range(len(left_bond)):
            if np.array_equal(left_bond[k], right_bond[k]) == False:
                return True
            
            if k == len(left_bond)-1:
                for l in range(len(left)):
                    if left[l].replace("@","") != right[l].replace("@",""):
                        return False
                    
                return True

    def removeFromLeftLists(self, remove_index_left, index):
        to_be_deleted_left = [self.left_counters,
                            self.left_charge_counters,
                            self.left_bond_counters,
                            self.left_stereo_counters]
  
        # Delete left
        for c_list in to_be_deleted_left:
            c_list[index] = np.delete(c_list[index], remove_index_left, axis=0)

    def removeFromRightLists(self, remove_index_right, index):
        to_be_deleted_right = [self.right_counters,
                              self.right_charge_counters,
                              self.right_bond_counters,
                              self.right_stereo_counters]

        # Delete right
        for c_list in to_be_deleted_right:
            c_list[index] = np.delete(c_list[index], remove_index_right, axis=0)



    def sortLeftLists(self, order, index):
        to_be_sorted_left = [self.left_counters,
                            self.left_charge_counters,
                            self.left_bond_counters,
                            self.left_stereo_counters]

        for c_list in to_be_sorted_left:
            c_list[index] = c_list[index][order]

        

    def sortRightLists(self, order, index):
        to_be_sorted_right = [self.right_counters,
                            self.right_charge_counters,
                            self.right_bond_counters,
                            self.right_stereo_counters]

        for c_list in to_be_sorted_right:
            c_list[index] = c_list[index][order]

    def removeSmallMolecules(self, left, right, index):
        mol_list = ['[O-][H]', '[H]O[H]', '[H][O+]([H])[H]']
        remove_indices = []
        for i in range(len(left)):
            if left[i] == right[i]:
                continue
            
            if left[i] in mol_list and right[i] in mol_list:
                remove_indices.append(i)

        left = np.delete(left, remove_indices, axis=0)
        right = np.delete(right, remove_indices, axis=0)
        self.removeFromLeftLists(remove_indices, index)
        self.removeFromRightLists(remove_indices, index)

        return left, right

    def checkEmpthy(self, left, right):
        if len(left) == 0 or len(right) == 0:
            return True
        else:
            return False

    def getRemoveIndices(self, mol, c1, c2, smi1, smi2):
        identical_indices_left = np.where(c2 == mol)[0]
        identical_indices_right = np.where(c1 == mol)[0]

        tmp_right = smi1[identical_indices_right]
        tmp_left = smi2[identical_indices_left]
        indices_to_move = identical_indices_right[np.isin(tmp_right,tmp_left)]

        match_indices = np.concatenate([np.setdiff1d(identical_indices_right,indices_to_move), indices_to_move])

        return match_indices

    def fixOthers(self, left, right, left_counter, right_counter, index, f_tmp):
        left = np.array(left)
        right = np.array(right)

        # Convert to Numpy
        left_c = [str(x.tolist()) for x in left_counter]
        left_c = np.array(left_c)

        right_c = [str(x.tolist()) for x in right_counter]
        right_c = np.array(right_c)

        # Counters
        tmp_cl = Counter(left_c)
        tmp_cr = Counter(right_c)

        # Compute difference of counters
        diff_c_right = tmp_cr - tmp_cl

        # Right
        remove_index = []
        for mol, mol_count in diff_c_right.most_common():
            match_indices = self.getRemoveIndices(mol, right_c, left_c, right, left)
            remove_index.append(match_indices[:mol_count])
        
        if len(remove_index) == 0:
            order = right_c.argsort()
            tmp_right = right[order]
            self.sortRightLists(order, index)
            remove_index_right = []
        else:
            remove_index_right = np.concatenate(remove_index)
            tmp_right = np.delete(right, remove_index_right, axis=0)
            tmp_right_c = np.delete(right_c, remove_index_right, axis=0)
            self.removeFromRightLists(remove_index_right, index)
            
            order = tmp_right_c.argsort()
            self.sortRightLists(order, index)
            tmp_right = tmp_right[order]

        # Left
        diff_c_left = tmp_cl - tmp_cr

        remove_index = []
        for mol, mol_count in diff_c_left.most_common():
            match_indices = self.getRemoveIndices(mol, left_c, right_c, left, right)
            remove_index.append(match_indices[:mol_count])
        
        if len(remove_index) == 0:
            order = left_c.argsort()
            tmp_left = left[order]
            self.sortLeftLists(order, index)
            remove_index_left = []
        else:
            remove_index_left = np.concatenate(remove_index)
            tmp_left = np.delete(left, remove_index_left, axis=0)
            tmp_left_c = np.delete(left_c, remove_index_left, axis=0)
            self.removeFromLeftLists(remove_index_left, index)
            
            order = tmp_left_c.argsort()
            self.sortLeftLists(order, index)
            tmp_left = tmp_left[order]

        return tmp_left, tmp_right

        # if len(tmp_left) == len(tmp_right):
        #     return True
        # else:
        #     return False

    def examineFails(self, sarray, rarray):
        f_tmp = open("f_tmp.txt","w+")
        count_tmp = 0

        combined_fails = np.concatenate([self.equal_size_fail, self.diff_size_fail])

        # Equal fails
        # for index in self.equal_size_fail:
        # for index in self.diff_size_fail:
        for index in combined_fails:
            left = np.array(sarray[index][0])
            right = np.array(sarray[index][1])
            left_c = self.left_counters[index]
            right_c = self.right_counters[index]

            if len(left_c) == len(right_c): # Equal
                for j in range(len(left_c)):
                    if np.array_equal(left_c[j],right_c[j]) == False:
                        count_tmp += 1
                        left, right = self.fixOthers(left, right, left_c, right_c, index, f_tmp)
                        break
            else: # Diff
                left, right = self.fixOthers(left, right, left_c, right_c, index, f_tmp)
                
            left, right = self.removeSmallMolecules(left, right, index)

            is_empthy = self.checkEmpthy(left, right)
            if is_empthy == True:
                self.empthy_fail.append(index)
                continue

            # Check if corrected is true
            is_corrected = self.checkCorrected(left, right)
            if is_corrected == True:
                self.count_true += 1
                self.f_true.write(str(rarray[index][0])  + "\n")
                continue
            
            # Check for coordinate bond
            is_coor_fail = self.checkCoordinate(rarray[index])
            if is_coor_fail in [1,2]: # Two situations
                is_fail = True
                if is_coor_fail == 2:
                    if self.checkCharge(left, right, left_charges, right_charges) == True:
                        is_fail = True
                    elif self.checkStereo(left_stereo, right_stereo, left, right) == True:
                        is_fail = False

                if is_fail == True:
                    self.coor_fail.append(index)
                    continue
            
            # Check charges
            left_charges = self.left_charge_counters[index]
            right_charges = self.right_charge_counters[index]

            is_charge_fail = self.checkCharge(left, right, left_charges, right_charges)
            if is_charge_fail == True:
                f_tmp.write(str(list(rarray[index]))  + "\n")
                f_tmp.write(str(list(left)) + "\n")
                f_tmp.write(str(list(right)) + "\n")
                self.charge_fail.append(index)
                continue

            # Check bonds
            left_bonds = self.left_bond_counters[index]
            right_bonds = self.right_bond_counters[index]

            is_bond_fail = self.checkBond(left_bonds, right_bonds)
            if is_bond_fail == True:
                self.bond_fail.append(index)
                continue
            
            # Check stereo
            left_stereo = self.left_stereo_counters[index]
            right_stereo = self.right_stereo_counters[index]
            is_stereo_fail = self.checkStereo(left_stereo, right_stereo, left, right)
            if is_stereo_fail == True:
                self.f_true.write(str(rarray[index][0])  + "\n") # Is not counted as true                
                self.stereo_fails.append(index)
                continue
            
            # Uncategorised fails
            self.uncategorised_fails.append(index)
 
        f_tmp.close()

   
    def examineDiff(self, sarray, rarray):
        f_tmp = open("f_tmp.txt","w+")
        f_tmp_fname = open("f_tmp_fname.txt","w+")

        count_tmp = 0
        for i in range(len(self.diff_size)):
            index = self.diff_size[i]
            left = np.array(sarray[index][0])
            right = np.array(sarray[index][1])
                
            tmp_cl = Counter(left)
            tmp_cr = Counter(right)

            diff_c_right = tmp_cr - tmp_cl
            diff_c_left = tmp_cl - tmp_cr

            if len(right) > len(left):
                remove_index = []
                for mol, mol_count in diff_c_right.most_common():
                    remove_index.append(np.where(right == mol)[0][:mol_count])
                
                remove_index = np.concatenate(remove_index)

                tmp_right = np.delete(right, remove_index, axis=0)
                
                if len(left) == len(tmp_right):
                    count_tmp += 1
                    self.f_true.write(str(rarray[index][0])  + "\n")
                    self.diff_true_indices.append(index)
                    self.count_true += 1
                else:
                    self.diff_size_fail.append(index)
                    f_tmp_fname.write(str(list(rarray[index])[0]) + ";" + list(rarray[index])[1] + "\n")

                    f_tmp.write(str(list(rarray[index]))  + "\n")
                    f_tmp.write(str(list(left)) + "\n")
                    f_tmp.write(str(list(right)) + "\n")
                    f_tmp.write("-------------- \n")
            
            if len(left) > len(right):
                remove_index = []
                for mol, mol_count in diff_c_left.most_common():
                    remove_index.append(np.where(left == mol)[0][:mol_count])
                
                remove_index = np.concatenate(remove_index)

                tmp_left = np.delete(left, remove_index, axis=0)
                
                if len(right) == len(tmp_left):
                    count_tmp += 1
                    self.f_true.write(str(rarray[index][0])  + "\n")
                    self.diff_true_indices.append(index)
                    self.count_true += 1
                else:
                    self.diff_size_fail.append(index)
                    f_tmp_fname.write(str(list(rarray[index])[0]) + ";" + list(rarray[index])[1] + "\n")

                    f_tmp.write(str(list(rarray[index]))  + "\n")
                    f_tmp.write(str(list(left)) + "\n")
                    f_tmp.write(str(list(right)) + "\n")
                    f_tmp.write("-------------- \n")

        f_tmp.close()
        f_tmp_fname.close()

    def saveFails(self, rarray):
        save_lists = [[self.empthy_fail, self.f_empthy_fail],
                     [self.charge_fail,self.f_charge_fail],
                     [self.coor_fail,self.f_coor_fail],
                     [self.bond_fail,self.f_bond_fail],
                     [self.uncategorised_fails,self.f_uncategorised_fail]]

        for save_list in save_lists:
            to_save_list = save_list[0]
            f_save = save_list[1]

            for index in to_save_list:
                left_rf = rarray[index][0]
                right_rf = rarray[index][1]
                f_save.write(str(left_rf) + ";" + str(right_rf) + "\n")

    def printStats(self, sarray, rarray):
        print("Nr of possible comparisons: " + str(len(sarray)))
        print("Nr of true matches: " + str(self.count_true))
        self.f_nr_of_matches.write(str(self.count_true) + "\n")
        print("")
        print("Nr. of smiles with equal nr. of molecules: " + str(len(sarray) - len(self.diff_size)))
        print("Nr. of smiles with different nr. of molecules: " + str(len(self.diff_size)))
        print("")
        total_fails = len(self.empthy_fail) + len(self.charge_fail) + len(self.coor_fail) + len(self.bond_fail) + len(self.stereo_fails) + len(self.uncategorised_fails)
        print("Nr. of failed validations " + str(total_fails))
        print("   Empthy fails   " + str(len(self.empthy_fail)))
        print("   Coor fails     " + str(len(self.coor_fail)))
        print("   Charge fails   " + str(len(self.charge_fail)))
        print("   Bond fails   " + str(len(self.bond_fail)))
        print("   Stereo conversion error   " + str(len(self.stereo_fails)))
        print("   Uncategorised fails   " + str(len(self.uncategorised_fails)))
        print("")
        print("Nr. of failed validations excluding stereo chemistry " + str(total_fails - len(self.stereo_fails)))
        print("")
        print("Tmp cpunt " + str(self.count_tmp))

        self.saveFails(rarray)
            
        self.f_empthy_fail.close()
        self.f_failed.close()
        self.f_coor_fail.close()
        self.f_charge_fail.close()
        self.f_bond_fail.close()
        self.f_true.close()
        self.f_nr_of_matches.close()

    def printLatexFails(self, sarray, rarray):
        # \newcommand{\mtone}{M1}
        total_fails = len(self.empthy_fail) + len(self.charge_fail) + len(self.coor_fail) + len(self.bond_fail) + len(self.stereo_fails) + len(self.uncategorised_fails)

        f_latex = open("latex_fail_counts.tex", "w+")
        f_latex.write("\\newcommand{\\nrtruematches}{" + str(self.count_true) + "}\n")
        

        f_latex.write("\n")

        f_latex.write("\\newcommand{\\nrtotalfails}{" + str(total_fails) + "}\n")
        f_latex.write("\\newcommand{\\nremptyfails}{" + str(len(self.empthy_fail)) + "}\n")
        f_latex.write("\\newcommand{\\nrcoorfails}{" + str(len(self.coor_fail)) + "}\n")
        f_latex.write("\\newcommand{\\nrchargefails}{" + str(len(self.charge_fail)) + "}\n")
        f_latex.write("\\newcommand{\\nrbondfails}{" + str(len(self.bond_fail)) + "}\n")
        f_latex.write("\\newcommand{\\nrstereofails}{" + str(len(self.stereo_fails)) + "}\n")
        f_latex.write("\\newcommand{\\nruncategorisedfails}{" + str(len(self.uncategorised_fails)) + "}\n")

        f_latex.close()
